package com.bhrother.zamro.pim.batch;

import com.bhrother.zamro.pim.AbstractZamroPimApplicationTests;
import com.bhrother.zamro.pim.dto.CSVProductDTO;
import org.junit.Test;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.test.StepScopeTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static com.bhrother.zamro.pim.StaticTestMethods.getTestResourceFullNameFor;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        StepScopeTestExecutionListener.class
})
public class ProductBatchConfigTest extends AbstractZamroPimApplicationTests {


    @Autowired
    private ProductBatchConfig batchConfig;

    private String productCsvFile = getTestResourceFullNameFor("files/MinimalProductData.csv");

    @Test
    public void should_retrieve_product_reader() {
        FlatFileItemReader<CSVProductDTO> csvProductReader = batchConfig.csvProductReader(productCsvFile);
        assertNotNull(csvProductReader);
    }

    @Test
    public void should_retrieve_product_reader_even_with_empty_filename() {
        FlatFileItemReader<CSVProductDTO> csvProductReader = batchConfig.csvProductReader("");
        assertNotNull(csvProductReader);
    }

    @Test
    public void should_read_product_from_file() throws Exception {
        ExecutionContext executionContext = new ExecutionContext();
        FlatFileItemReader<CSVProductDTO> csvProductReader = batchConfig.csvProductReader(productCsvFile);

        csvProductReader.open(executionContext);

        CSVProductDTO csvProductDTO = csvProductReader.read();
        assertNotNull(csvProductDTO);
        assertEquals("4EDA0", csvProductDTO.getZamroId());
        assertEquals("Rechte inschroefkoppeling GE35LR1.1/4 -M+D (zonder moer en snijring) RVS-316TI", csvProductDTO.getName());
    }
}