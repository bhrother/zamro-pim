package com.bhrother.zamro.pim.batch;

import com.bhrother.zamro.pim.batch.processor.CategoryProcessor;
import com.bhrother.zamro.pim.dto.CSVCategoryDTO;
import com.bhrother.zamro.pim.model.Category;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CategoryProcessorTest {

    private CategoryProcessor categoryProcessor;

    @Before
    public void setup() {
        categoryProcessor = new CategoryProcessor();
    }

    @Test
    public void should_convert_property_dto_into_model() {
        CSVCategoryDTO CSVCategoryDTO = new CSVCategoryDTO.Builder()
                .categoryId(100L)
                .name("CategoryA")
                .build();

        Category category = categoryProcessor.process(CSVCategoryDTO);

        assertEquals(CSVCategoryDTO.getCategoryId(), category.getCode());
        assertEquals(CSVCategoryDTO.getName(), category.getName());
    }
}