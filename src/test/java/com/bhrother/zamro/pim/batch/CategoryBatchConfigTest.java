package com.bhrother.zamro.pim.batch;

import com.bhrother.zamro.pim.AbstractZamroPimApplicationTests;
import com.bhrother.zamro.pim.dto.CSVCategoryDTO;
import org.junit.Test;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.test.StepScopeTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static com.bhrother.zamro.pim.StaticTestMethods.getTestResourceFullNameFor;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        StepScopeTestExecutionListener.class
})
public class CategoryBatchConfigTest extends AbstractZamroPimApplicationTests {

    @Autowired
    private CategoryBatchConfig batchConfig;

    private String categoryCSVFile = getTestResourceFullNameFor("files/MinimalCategoryData.csv");

    @Test
    public void should_retrieve_category_reader() {
        FlatFileItemReader<CSVCategoryDTO> csvCategoryReader = batchConfig.csvCategoryReader(categoryCSVFile);
        assertNotNull(csvCategoryReader);
    }

    @Test
    public void should_retrieve_category_reader_even_with_empty_filename() {
        FlatFileItemReader<CSVCategoryDTO> csvCategoryReader = batchConfig.csvCategoryReader("");
        assertNotNull(csvCategoryReader);
    }

    @Test
    public void should_read_category_from_file() throws Exception {
        ExecutionContext executionContext = new ExecutionContext();
        FlatFileItemReader<CSVCategoryDTO> csvCategoryReader = batchConfig.csvCategoryReader(categoryCSVFile);

        csvCategoryReader.open(executionContext);

        CSVCategoryDTO csvCategoryDTO = csvCategoryReader.read();
        assertNotNull(csvCategoryDTO);
        assertEquals(Long.valueOf(48622), csvCategoryDTO.getCategoryId());
        assertEquals("Decoupeerzagen en recipro zaagbladen", csvCategoryDTO.getName());
    }

}