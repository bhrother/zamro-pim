package com.bhrother.zamro.pim.batch;

import com.bhrother.zamro.pim.ProductTestHelper;
import com.bhrother.zamro.pim.batch.processor.ProductProcessor;
import com.bhrother.zamro.pim.dto.CSVProductDTO;
import com.bhrother.zamro.pim.exception.CategoryNotFoundException;
import com.bhrother.zamro.pim.exception.InvalidCSVProductException;
import com.bhrother.zamro.pim.model.Category;
import com.bhrother.zamro.pim.model.Product;
import com.bhrother.zamro.pim.service.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class ProductProcessorTest {

    private ProductProcessor productProcessor;
    private Category validCategory;

    private static final Long VALID_CATEGORY_ID = 2L;

    @Before
    public void setup() {
        validCategory = new Category();
        validCategory.setId(1L);
        validCategory.setCode(VALID_CATEGORY_ID);
        validCategory.setName("ValidCategoryB");

        CategoryService categoryService = Mockito.mock(CategoryService.class);
        productProcessor = new ProductProcessor(categoryService);

        when(categoryService.findByCode(2L)).thenReturn(validCategory);
        when(categoryService.findByCode(1L)).thenThrow(CategoryNotFoundException.class);
        when(categoryService.findByCode(null)).thenThrow(CategoryNotFoundException.class);
    }

    @Test
    public void should_convert_dto_into_model_properly() {
        CSVProductDTO CSVProductDTO = ProductTestHelper.returnValidCSVProductDTOBuilder()
                .categoryID(VALID_CATEGORY_ID)
                .zamroId("ZAMRO1")
                .build();

        Product product = productProcessor.process(CSVProductDTO);

        assertNotNull(product);
        assertEquals(CSVProductDTO.getZamroId(), product.getZamroID());
        assertEquals(CSVProductDTO.getName(), product.getName());
        assertEquals(CSVProductDTO.getDescription(), product.getDescription());
        assertEquals(CSVProductDTO.getPurchasePrice(), product.getPurchasePrice());
        assertEquals(CSVProductDTO.getMinOrderQuantity(), product.getMinOrderQuantity());
        assertEquals(CSVProductDTO.getUnitOfMeasure(), product.getUnitOfMeasure());
        assertEquals(CSVProductDTO.getAvailable() == 1, product.getAvailable());
        assertEquals(validCategory.getId(), product.getCategory().getId());
    }

    @Test
    public void should_convert_dto_into_model_properly_removinh_html_tags() {
        CSVProductDTO CSVProductDTO = ProductTestHelper.returnValidCSVProductDTOBuilder()
                .categoryID(VALID_CATEGORY_ID)
                .zamroId("ZAMRO1")
                .name("<br>product with html tags</br>")
                .build();

        Product product = productProcessor.process(CSVProductDTO);

        assertNotNull(product);
        assertEquals(CSVProductDTO.getZamroId(), product.getZamroID());
        assertEquals("product with html tags", product.getName());
        assertEquals(CSVProductDTO.getDescription(), product.getDescription());
        assertEquals(CSVProductDTO.getPurchasePrice(), product.getPurchasePrice());
        assertEquals(CSVProductDTO.getMinOrderQuantity(), product.getMinOrderQuantity());
        assertEquals(CSVProductDTO.getUnitOfMeasure(), product.getUnitOfMeasure());
        assertEquals(CSVProductDTO.getAvailable() == 1, product.getAvailable());
        assertEquals(validCategory.getId(), product.getCategory().getId());
    }

    @Test(expected = InvalidCSVProductException.class)
    public void should_throw_invalidcsvproductexception_if_invalid_fields() {
        CSVProductDTO CSVProductDTO = new CSVProductDTO.Builder()
                .description("Invalid product with incomplete information")
                .build();
        productProcessor.process(CSVProductDTO);
    }
}