package com.bhrother.zamro.pim.api;

import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.io.FileInputStream;

import static com.bhrother.zamro.pim.StaticTestMethods.getTestResourceFullNameFor;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BatchJobController.class)
public class BatchJobControllerTest extends AbstractControllerTest{

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JobLauncher jobLauncher;

    @MockBean(name = "csvProductFileToDatabaseJob")
    private Job csvProductFileToDatabaseJob;

    @MockBean(name = "csvCategoryFileToDatabaseJob")
    private Job csvCategoryFileToDatabaseJob;

    @Before
    public void setup() {
    }

    @Test
    public void should_upload_products() throws Exception {
        String fileName = getTestResourceFullNameFor("files/MinimalProductData.csv");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "MinimalProductData.csv", "text/csv", new FileInputStream(fileName));

        mockMvc.perform(multipart("/batch/products").file(multipartFile))
                .andExpect(status().is(204));
        verify(jobLauncher, atLeastOnce()).run(eq(csvProductFileToDatabaseJob), any(JobParameters.class));
    }

    @Test
    public void should_upload_category() throws Exception {
        String fileName = getTestResourceFullNameFor("files/MinimalCategoryData.csv");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "MinimalCategoryData.csv", "text/csv", new FileInputStream(fileName));

        mockMvc.perform(multipart("/batch/categories").file(multipartFile))
                .andExpect(status().is(204));
        verify(jobLauncher, atLeastOnce()).run(eq(csvCategoryFileToDatabaseJob), any(JobParameters.class));
    }

    @Test
    public void should_try_upload_products_but_return_bad_request_for_empty_files() throws Exception {
        mockMvc.perform(multipart("/batch/products").file("file", null))
                .andExpect(status().is(400));
    }

    @Test
    public void should_try_upload_category_but_return_bad_request_for_empty_files() throws Exception {
        mockMvc.perform(multipart("/batch/categories").file("file", null))
                .andExpect(status().is(400));
    }

    @Test
    public void should_try_upload_product_but_return_400_for_invalid_file() throws Exception {
        String fileName = getTestResourceFullNameFor("files/InvalidFile.txt");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "InvalidFile.txt", "text/txt", new FileInputStream(fileName));

        mockMvc.perform(multipart("/batch/products").file(multipartFile))
                .andExpect(status().is(400));
        verify(jobLauncher, never()).run(eq(csvProductFileToDatabaseJob), any(JobParameters.class));
    }
}