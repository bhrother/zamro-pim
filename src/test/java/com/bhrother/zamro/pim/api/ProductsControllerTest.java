package com.bhrother.zamro.pim.api;

import com.bhrother.zamro.pim.dto.CategoryDTO;
import com.bhrother.zamro.pim.dto.IdsDTO;
import com.bhrother.zamro.pim.dto.ProductDTO;
import com.bhrother.zamro.pim.exception.ProductNotFoundException;
import com.bhrother.zamro.pim.model.Product;
import com.bhrother.zamro.pim.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static com.bhrother.zamro.pim.ProductTestHelper.returnValidProductDTOBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductsController.class)
public class ProductsControllerTest extends AbstractControllerTest {

    private static final Long VALID_ID = 1L;
    private static final Long INVALID_ID = -1L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Before
    public void setup() {
        Product product = returnValidProductDTOBuilder().build().toProduct();
        product.setId(VALID_ID);
        product.setAvailable(true);

        List<Product> products = new ArrayList<>();
        products.add(product);

        when(productService.getPage(any(Pageable.class))).thenReturn(new PageImpl<>(products));
        when(productService.getById(VALID_ID)).thenReturn(product);
        when(productService.getById(INVALID_ID)).thenThrow(ProductNotFoundException.class);
        when(productService.create(any(Product.class))).thenReturn(product);
    }

    @Test
    public void should_get_products() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/products?page=0"))
                .andReturn();
        assert !mvcResult.getResponse().getContentAsString().isEmpty();
        assert mvcResult.getResponse().getStatus() == 200;
    }

    @Test
    public void should_get_product_by_id() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/products/1"))
                .andReturn();
        ProductDTO productDTO = getResponseAsEntity(ProductDTO.class, mvcResult.getResponse());
        assertNotNull(productDTO);
        assertEquals(VALID_ID, productDTO.getId());
    }

    @Test
    public void should_try_get_product_by_id_and_return_404_if_not_found() throws Exception {
        mockMvc.perform(get("/products/" + INVALID_ID))
                .andExpect(status().is(404));
    }

    @Test
    public void should_create_product() throws Exception {
        ProductDTO productDTO = returnValidProductDTOBuilder()
                .categoryDTO(new CategoryDTO.Builder().id(1L).build())
                .zamroId("001")
                .build();
        String productAsString = objectToJSONString(productDTO);
        mockMvc.perform(post("/products")
                .content(productAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201));
    }

    @Test
    public void should_update_product() throws Exception {
        ProductDTO productDTO = returnValidProductDTOBuilder()
                .categoryDTO(new CategoryDTO.Builder().id(1L).build())
                .zamroId("002")
                .build();
        String productAsString = objectToJSONString(productDTO);
        mockMvc.perform(put("/products/"+VALID_ID)
                .content(productAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));
    }

    @Test
    public void should_delete_product() throws Exception {
        mockMvc.perform(delete("/products/"+VALID_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));
    }

    @Test
    public void should_enable_product_again() throws Exception {
        ProductDTO productDTO = returnValidProductDTOBuilder()
                .categoryDTO(new CategoryDTO.Builder().id(1L).build())
                .zamroId("002")
                .build();
        String productAsString = objectToJSONString(productDTO);
        mockMvc.perform(patch("/products/"+VALID_ID+"/enable")
                .content(productAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));
    }

    @Test
    public void should_download_products_as_csv() throws Exception {
        IdsDTO idsDTO = new IdsDTO();
        idsDTO.setIds(Sets.newSet(1L, 2L, 3L, VALID_ID));

        String expectedResult = "\"ZamroID\",\"Name\",\"Description\",\"MinOrderQuantity\",\"UnitOfMeasure\",\"CategoryID\",\"PurchasePrice\",\"Available\"\n" +
                "\"ZAMRO0\",\"ProductA\",\"Valid product with valid category\",\"1\",\"PKG\",\"1\",\"1\",\"1\"\n";

        mockMvc.perform(post("/products/download/csv")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectToJSONString(idsDTO)))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/csv"))
                .andExpect(content().string(expectedResult));
    }

    @Test
    public void should_download_products_as_xls() throws Exception {
        IdsDTO idsDTO = new IdsDTO();
        idsDTO.setIds(Sets.newSet(1L, 2L, 3L, VALID_ID));

        mockMvc.perform(post("/products/download/xls")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectToJSONString(idsDTO)))
                .andExpect(status().is(200))
                .andExpect(content().contentType("application/vnd.ms-excel"));
    }

    @Test
    public void should_download_products_as_xlsx() throws Exception {
        IdsDTO idsDTO = new IdsDTO();
        idsDTO.setIds(Sets.newSet(1L, 2L, 3L, VALID_ID));

        mockMvc.perform(post("/products/download/xlsx")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectToJSONString(idsDTO)))
                .andExpect(status().is(200))
                .andExpect(content().contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    }

    @Test
    public void should_download_products_as_invalidformat_and_return_400() throws Exception {
        IdsDTO idsDTO = new IdsDTO();
        idsDTO.setIds(Sets.newSet(1L, 2L, 3L, VALID_ID));

        mockMvc.perform(post("/products/download/invalidformat")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectToJSONString(idsDTO)))
                .andExpect(status().is(400));
    }
}