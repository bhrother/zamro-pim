package com.bhrother.zamro.pim.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@EnableSpringDataWebSupport
public abstract class AbstractControllerTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    <T> T getResponseAsEntity(Class<T> tClass, MockHttpServletResponse response) {
        try {
            return objectMapper.readValue(response.getContentAsByteArray(), tClass);
        } catch (IOException e) {
            return null;
        }
    }

    String objectToJSONString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
