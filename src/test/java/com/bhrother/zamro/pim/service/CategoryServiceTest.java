package com.bhrother.zamro.pim.service;

import com.bhrother.zamro.pim.AbstractZamroPimApplicationTests;
import com.bhrother.zamro.pim.exception.CategoryNotFoundException;
import com.bhrother.zamro.pim.model.Category;
import com.bhrother.zamro.pim.repository.CategoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

public class CategoryServiceTest extends AbstractZamroPimApplicationTests {

    private static final Long INVALID_CODE = -1L;
    private final Long VALID_CODE = 100L;

    @SpyBean
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    private static boolean initialized = false;

    @Before
    public void setup() {
        if (!initialized) {
            Category category = new Category(null, VALID_CODE, "Category A", null);
            categoryRepository.save(category);
            initialized = true;
        }
    }

    @Test(expected = CategoryNotFoundException.class)
    public void should_throw_categorynotfoundexception_if_invalid_category() {
        categoryService.findByCode(INVALID_CODE);
    }

    @Test
    public void should_return_category() {
        Category category = categoryService.findByCode(VALID_CODE);
        assertNotNull(category);
        assertEquals(VALID_CODE, category.getCode());
    }

    @Test
    public void should_only_query_category_once() {
        categoryService.findByCode(VALID_CODE);
        verify(categoryService).findByCode(VALID_CODE);

        reset(categoryService);
        categoryService.findByCode(VALID_CODE);
        categoryService.findByCode(VALID_CODE);
        categoryService.findByCode(VALID_CODE);
        verify(categoryService, Mockito.times(0)).findByCode(VALID_CODE);
    }
}