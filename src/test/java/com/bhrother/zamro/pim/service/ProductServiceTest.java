package com.bhrother.zamro.pim.service;

import com.bhrother.zamro.pim.exception.ProductNotFoundException;
import com.bhrother.zamro.pim.model.Product;
import com.bhrother.zamro.pim.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

public class ProductServiceTest {

    private static final Long VALID_PRODUCT_ID = 1L;
    private static final Long INVALID_PRODUCT_ID = -1L;

    private ProductService productService;
    private ProductRepository productRepository;

    @Before
    public void setup() {
        Product product = new Product();
        product.setId(VALID_PRODUCT_ID);
        product.setAvailable(true);

        List<Product> productList = new ArrayList<>();
        productList.add(product);

        productRepository = Mockito.mock(ProductRepository.class);
        productService = new ProductService(productRepository);

        when(productRepository.findById(VALID_PRODUCT_ID)).thenReturn(Optional.of(product));
        when(productRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(productList));
    }

    @Test(expected = ProductNotFoundException.class)
    public void should_return_productnotfoundexception_when_product_not_in_db() {
        productService.getById(INVALID_PRODUCT_ID);
    }

    @Test
    public void should_retrieve_product_from_db() {
        Product product = productService.getById(VALID_PRODUCT_ID);
        assertNotNull(product);
        assertEquals(VALID_PRODUCT_ID, product.getId());
    }

    @Test
    public void should_retrieve_products_paged() {
        Page<Product> productPage = productService.getPage(PageRequest.of(0, 20));
        assertNotNull(productPage);
        assertEquals(1, productPage.getTotalElements());
        assertEquals(1, productPage.getTotalPages());
        assertEquals(VALID_PRODUCT_ID, productPage.getContent().get(0).getId());
    }

    @Test
    public void should_disable_product() {
        Product product = new Product();
        product.setId(20L);
        product.setAvailable(true);
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product productDisabled = productService.disableProduct(product.getId());
        assertFalse(productDisabled.getAvailable());
    }

    @Test
    public void should_enable_product() {
        Product product = new Product();
        product.setId(20L);
        product.setAvailable(false);
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product productDisabled = productService.enableProduct(product.getId());
        assertTrue(productDisabled.getAvailable());
    }

    @Test
    public void should_not_disable_product_if_already_disabled() {
        Product product = new Product();
        product.setId(20L);
        product.setAvailable(false);
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));

        Product productDisabled = productService.disableProduct(product.getId());
        assertFalse(productDisabled.getAvailable());

        verify(productRepository, never()).save(any(Product.class));
    }

    @Test
    public void should_create_product_ignoring_id_provided() {
        Product productTryingSave = new Product();
        productTryingSave.setId(20L);
        productTryingSave.setAvailable(false);

        Product productEffectiveSaving = new Product();
        productTryingSave.setId(200L);
        productEffectiveSaving.setAvailable(false);

        when(productRepository.saveAndFlush(argThat(product -> product.getId() == null))).thenReturn(productEffectiveSaving);

        Product product = productService.create(productTryingSave);
        assertEquals(productEffectiveSaving, product);
    }

    @Test
    public void should_update_product_ignoring_id_provided() {
        Product productTryingSave = new Product();
        productTryingSave.setId(20L);
        productTryingSave.setAvailable(false);

        long ACTUAL_ID = 200L;
        Product productEffectiveSaving = new Product();
        productTryingSave.setId(ACTUAL_ID);
        productEffectiveSaving.setAvailable(false);

        when(productRepository.saveAndFlush(argThat(product -> product.getId().equals(ACTUAL_ID)))).thenReturn(productEffectiveSaving);

        Product product = productService.update(ACTUAL_ID, productTryingSave);
        assertEquals(productEffectiveSaving, product);
    }
}