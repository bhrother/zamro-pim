package com.bhrother.zamro.pim;

import java.util.Objects;

public class StaticTestMethods {

    public static String getTestResourceFullNameFor(String resourceName) {
        ClassLoader classLoader = StaticTestMethods.class.getClassLoader();
        return Objects.requireNonNull(classLoader.getResource(resourceName)).getFile();
    }
}
