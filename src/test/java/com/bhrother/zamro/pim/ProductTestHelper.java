package com.bhrother.zamro.pim;

import com.bhrother.zamro.pim.dto.CSVProductDTO;
import com.bhrother.zamro.pim.dto.CategoryDTO;
import com.bhrother.zamro.pim.dto.ProductDTO;

import java.math.BigDecimal;

public class ProductTestHelper {

    public static CSVProductDTO.Builder returnValidCSVProductDTOBuilder() {
        return new CSVProductDTO.Builder()
                .zamroId("ZAMRO0")
                .name("ProductA")
                .description("Valid csv product with valid category")
                .minOrderQuantity(BigDecimal.ONE)
                .purchasePrice(BigDecimal.ONE)
                .unitOfMeasure("PKG")
                .available(1);
    }

    public static ProductDTO.Builder returnValidProductDTOBuilder() {
        return new ProductDTO.Builder()
                .zamroId("ZAMRO0")
                .name("ProductA")
                .description("Valid product with valid category")
                .minOrderQuantity(BigDecimal.ONE)
                .purchasePrice(BigDecimal.ONE)
                .unitOfMeasure("PKG")
                .categoryDTO(new CategoryDTO.Builder().id(1L).code(1L).build())
                .available(true);
    }
}
