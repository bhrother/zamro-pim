package com.bhrother.zamro.pim.api.export;

import com.bhrother.zamro.pim.model.Product;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface ApiExporter {

    String getContentType();
    void export(HttpServletResponse httpServletResponse, List<Product> productsExport);
}
