package com.bhrother.zamro.pim.api.export;

import com.bhrother.zamro.pim.batch.ProductBatchConfig;
import com.bhrother.zamro.pim.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVExporter implements ApiExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CSVExporter.class);

    private static final char DEFAULT_SEPARATOR = ',';

    private static void writeLine(Writer w, List<String> values) {
        writeLine(w, values, DEFAULT_SEPARATOR, '"');
    }

    //https://tools.ietf.org/html/rfc4180
    private static String followCVSformat(String value) {

        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    private static void writeLine(Writer w, List<String> values, char separators, char customQuote) {

        boolean first = true;

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(separators);
            }
            if (customQuote == ' ') {
                sb.append(followCVSformat(value));
            } else {
                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }

            first = false;
        }
        sb.append("\n");
        try {
            w.append(sb.toString());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public String getContentType() {
        return "text/csv";
    }

    @Override
    public void export(HttpServletResponse httpServletResponse, List<Product> productsExport) {
        LOGGER.debug("Exporting selected products to CSV.");
        Writer writer;
        try {
            writer = httpServletResponse.getWriter();
        } catch (IOException e) {
            LOGGER.error("Error getting the writer of csv response", e);
            return;
        }

        writeLine(writer, Arrays.asList(ProductBatchConfig.HEADER));
        productsExport.stream().map(this::productToCSVFormat).forEach(lines -> writeLine(writer, lines));
        try {
            writer.close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private List<String> productToCSVFormat(Product product) {
        LOGGER.debug("Converting product {} to CSV format", product);
        List<String> list = new ArrayList<>();
        list.add(product.getZamroID());
        list.add(product.getName());
        list.add(product.getDescription());
        list.add(product.getMinOrderQuantity().toString());
        list.add(product.getUnitOfMeasure());
        list.add(product.getCategory().getCode().toString());
        list.add(product.getPurchasePrice().toString());
        list.add(product.getAvailable()?"1":"0");

        return list;
    }
}
