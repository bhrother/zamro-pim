package com.bhrother.zamro.pim.api;

import com.bhrother.zamro.pim.api.export.ApiExporter;
import com.bhrother.zamro.pim.api.export.CSVExporter;
import com.bhrother.zamro.pim.api.export.XlsExcelExporterImpl;
import com.bhrother.zamro.pim.api.export.XlsxExcelExporterImpl;
import com.bhrother.zamro.pim.dto.IdsDTO;
import com.bhrother.zamro.pim.dto.ProductDTO;
import com.bhrother.zamro.pim.exception.InvalidExportFormatException;
import com.bhrother.zamro.pim.model.Product;
import com.bhrother.zamro.pim.service.ProductService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/products")
@Api(value = "/products", description = "Operations about products", produces = "application/json")
public class ProductsController {

    private final Map<String, ApiExporter> exportFormats = new HashMap<>();

    private final ProductService productService;

    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @PostConstruct
    void initialize() {
        exportFormats.put("csv", new CSVExporter());
        exportFormats.put("xls", new XlsExcelExporterImpl());
        exportFormats.put("xlsx", new XlsxExcelExporterImpl());
    }

    @PostMapping
    @ApiOperation(value = "Create a new Product")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "")})
    public ResponseEntity create(@RequestBody ProductDTO productDTO) {
        productService.create(productDTO.toProduct());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update an existent Product")
    public ResponseEntity update(@PathVariable Long id, @RequestBody ProductDTO productDTO) {
        productService.update(id, productDTO.toProduct());
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/{id}/enable")
    @ApiOperation(value = "Enable a inactive product (deleted)")
    @ApiResponses(value = { @ApiResponse(code = 204, message = "")})
    public ResponseEntity enableProduct(@PathVariable Long id) {
        productService.enableProduct(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Disable a product (does not really delete)")
    @ApiResponses(value = { @ApiResponse(code = 204, message = "")})
    public ResponseEntity disableProduct(@PathVariable Long id) {
        productService.disableProduct(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find a product by Id")
    public ResponseEntity<ProductDTO> getById(@PathVariable Long id) {
        Product product = productService.getById(id);
        return ResponseEntity.ok(ProductDTO.fromProduct(product));
    }

    @GetMapping
    @ApiOperation(value = "Retrieve all products paginated")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " +
                            "Default sort order is ascending. " +
                            "Multiple sort criteria are supported.")
    })
    public ResponseEntity<Page<ProductDTO>> getAllPageable(Pageable pageable) {
        return ResponseEntity.ok(productService.getPage(pageable).map(ProductDTO::fromProduct));
    }

    @PostMapping("/download/{format}")
    @ApiOperation(value = "Download selected products by list of Ids in the specified format")
    public ResponseEntity exportTo(
            @ApiParam(value = "Format to download the products", allowableValues = "csv,xls,xlsx", required = true)
            @PathVariable("format") String format,
            @ApiParam(value = "IDs of products to download", required = true)
            @RequestBody IdsDTO productIds, HttpServletResponse httpResponse) throws IOException {
        ApiExporter apiExporter = exportFormats.get(format);

        if (apiExporter == null) {
            throw new InvalidExportFormatException(exportFormats.keySet());
        }
        httpResponse.setContentType(apiExporter.getContentType());

        List<Product> productListToExport = productIds.getIds()
                .stream()
                .map(productService::getById)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        apiExporter.export(httpResponse, productListToExport);
        return ResponseEntity.ok().build();
    }
}
