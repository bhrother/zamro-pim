package com.bhrother.zamro.pim.api;

import com.bhrother.zamro.pim.exception.InvalidMultipartFileException;
import com.bhrother.zamro.pim.exception.InvalidUploadFileException;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import static com.bhrother.zamro.pim.batch.CategoryBatchConfig.CATEGORY_FILE_NAME;
import static com.bhrother.zamro.pim.batch.ProductBatchConfig.PRODUCT_FILE_NAME;

@RestController
@RequestMapping("/batch")
@Api(value = "/batch", description = "Batch operations for uploading Categories and Products into db")
public class BatchJobController {

    private static final String UPLOAD_CONTENT_TYPE = "text/csv";
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsController.class);

    private final JobLauncher jobLauncher;

    private final Job csvProductFileToDatabaseJob;
    private final Job csvCategoryFileToDatabaseJob;

    @Autowired
    public BatchJobController(JobLauncher jobLauncher, Job csvProductFileToDatabaseJob, Job csvCategoryFileToDatabaseJob) {
        this.jobLauncher = jobLauncher;
        this.csvProductFileToDatabaseJob = csvProductFileToDatabaseJob;
        this.csvCategoryFileToDatabaseJob = csvCategoryFileToDatabaseJob;
    }

    @PostMapping("/products")
    @ApiOperation(value = "Upload products into DB")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "")})
    public ResponseEntity uploadProducts(
            @ApiParam(name = "file", format = "text/csv", required = true, value = "CSV file containing products to upload.")
            @RequestParam("file") MultipartFile multipartFile) {
        validateMultiparFile(multipartFile);
        LOGGER.info("Received product csv file {} to Upload with size: {} ", multipartFile.getOriginalFilename(), multipartFile.getSize());

        return executeBatchJob(csvProductFileToDatabaseJob, PRODUCT_FILE_NAME, multipartFile);
    }

    @PostMapping("/categories")
    @ApiOperation(value = "Upload categories into DB")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "")})
    public ResponseEntity uploadCategories(
            @ApiParam(name = "file", format = "text/csv", required = true, value = "CSV file containing categories to upload.")
            @RequestParam("file") MultipartFile multipartFile) {
        validateMultiparFile(multipartFile);
        LOGGER.info("Received category csv file {} to Upload with size: {} ", multipartFile.getOriginalFilename(), multipartFile.getSize());

        return executeBatchJob(csvCategoryFileToDatabaseJob, CATEGORY_FILE_NAME, multipartFile);
    }

    private void validateMultiparFile(MultipartFile multipartFile) {
        if (multipartFile == null || multipartFile.isEmpty()) {
            throw new InvalidMultipartFileException();
        }
        if (!multipartFile.getContentType().equals(UPLOAD_CONTENT_TYPE)) {
            throw new InvalidUploadFileException(UPLOAD_CONTENT_TYPE);
        }
    }

    private ResponseEntity executeBatchJob(Job job, String fileName, @RequestParam("file") MultipartFile multipartFile) {
        try {
            File tempFile = Files.createTempFile("", fileName).toFile();
            tempFile.deleteOnExit();
            multipartFile.transferTo(tempFile);

            Map<String,JobParameter> parameters = new HashMap<>();
            parameters.put("fileName", new JobParameter("file:"+tempFile.getAbsolutePath()));

            jobLauncher.run(job, new JobParameters(parameters));
            return ResponseEntity.noContent().build();

        } catch (JobInstanceAlreadyCompleteException ex) {
            return ResponseEntity.status(HttpStatus.OK).body("This job has been completed already!");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
