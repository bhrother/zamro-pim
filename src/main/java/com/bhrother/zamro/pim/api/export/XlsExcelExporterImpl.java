package com.bhrother.zamro.pim.api.export;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class XlsExcelExporterImpl extends AbstractExcelExporter {
    @Override
    Workbook createWorkbook() {
        return new HSSFWorkbook();
    }

    @Override
    public String getContentType() {
        return "application/vnd.ms-excel";
    }
}
