package com.bhrother.zamro.pim.api.export;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XlsxExcelExporterImpl extends AbstractExcelExporter {

    @Override
    Workbook createWorkbook() {
        return new XSSFWorkbook();
    }

    @Override
    public String getContentType() {
        return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
}
