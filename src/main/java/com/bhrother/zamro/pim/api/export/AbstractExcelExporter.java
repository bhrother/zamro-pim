package com.bhrother.zamro.pim.api.export;

import com.bhrother.zamro.pim.batch.ProductBatchConfig;
import com.bhrother.zamro.pim.model.Product;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public abstract class AbstractExcelExporter implements ApiExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExcelExporter.class);

    abstract Workbook createWorkbook();

    @Override
    public void export(HttpServletResponse httpServletResponse, List<Product> productsExport) {

        Workbook workbook = createWorkbook(); //new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Employee");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        //"ZamroID", "Name", "Description", "MinOrderQuantity", "UnitOfMeasure", "CategoryID", "PurchasePrice", "Available"
        for(int i = 0; i < ProductBatchConfig.HEADER.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(ProductBatchConfig.HEADER[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with product data
        int rowNum = 1;
        for(Product product: productsExport) {
            Row row = sheet.createRow(rowNum++);
            int lastCell = 0;

            row.createCell(lastCell++).setCellValue(product.getZamroID());
            row.createCell(lastCell++).setCellValue(product.getName());
            row.createCell(lastCell++).setCellValue(product.getDescription());
            row.createCell(lastCell++).setCellValue(product.getMinOrderQuantity().doubleValue());
            row.createCell(lastCell++).setCellValue(product.getUnitOfMeasure());
            row.createCell(lastCell++).setCellValue(product.getCategory().getCode());
            row.createCell(lastCell++).setCellValue(product.getPurchasePrice().doubleValue());
            row.createCell(lastCell++).setCellValue(product.getAvailable()?"1":"0");
        }

        // Resize all columns to fit the content size
        for(int i = 0; i < ProductBatchConfig.HEADER.length; i++) {
            sheet.autoSizeColumn(i);
        }
        // Write the output to a file
        try {
            workbook.write(httpServletResponse.getOutputStream());
        } catch (IOException e) {
            LOGGER.error("Error saving the Excel file to response outputstream", e);
        }

        // Closing the workbook
        try {
            workbook.close();
        } catch (IOException e) {
            LOGGER.error("Error closing the workbook", e);
        }
    }
}
