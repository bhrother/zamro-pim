package com.bhrother.zamro.pim.service;

import com.bhrother.zamro.pim.exception.ProductNotFoundException;
import com.bhrother.zamro.pim.model.Product;
import com.bhrother.zamro.pim.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getById(Long id) {
        return productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
    }

    public Page<Product> getPage(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    public Product create(Product product) {
        product.setId(null);
        return productRepository.saveAndFlush(product);
    }

    public Product update(Long id, Product product) {
        product.setId(id);
        return productRepository.saveAndFlush(product);
    }

    public Product disableProduct(Long id) {
        return updateAvailability(id, false);
    }

    public Product enableProduct(Long id) {
        return updateAvailability(id, true);
    }

    private Product updateAvailability(Long id, Boolean available) {
        Product product = getById(id);
        if (product.getAvailable() != available) {
            product.setAvailable(available);
            return productRepository.save(product);
        }
        return product;
    }
}
