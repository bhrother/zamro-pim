package com.bhrother.zamro.pim.service;

import com.bhrother.zamro.pim.cache.CacheConfig;
import com.bhrother.zamro.pim.exception.CategoryNotFoundException;
import com.bhrother.zamro.pim.model.Category;
import com.bhrother.zamro.pim.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Cacheable(cacheNames = CacheConfig.CATEGORY_CACHE_NAME)
    public Category findByCode(Long code) {
        return categoryRepository.findByCode(code).orElseThrow(CategoryNotFoundException::new);
    }
}
