package com.bhrother.zamro.pim.cache;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CacheConfig {

    public static final String CATEGORY_CACHE_NAME = "categoryCache";
}
