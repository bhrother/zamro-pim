package com.bhrother.zamro.pim.swagger;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Profile("!test")
@Configuration
@EnableSwagger2
public class SwaggerConfig {

}
