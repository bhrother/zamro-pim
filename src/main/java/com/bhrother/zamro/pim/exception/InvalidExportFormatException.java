package com.bhrother.zamro.pim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Set;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidExportFormatException extends RuntimeException{

    public InvalidExportFormatException(Set<String> formats) {
        super("Allowed formats: " + String.join(", ", formats));
    }
}
