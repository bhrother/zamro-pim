package com.bhrother.zamro.pim.exception;

public class CategoryNotFoundException extends RuntimeException {

    private static final String message = "Category not found";

    public CategoryNotFoundException(){
        super(message);
    }
}
