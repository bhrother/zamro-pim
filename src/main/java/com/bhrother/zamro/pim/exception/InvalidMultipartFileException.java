package com.bhrother.zamro.pim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidMultipartFileException extends RuntimeException {

    public InvalidMultipartFileException() {
        super();
    }
}
