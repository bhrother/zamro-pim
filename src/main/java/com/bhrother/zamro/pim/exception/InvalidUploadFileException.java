package com.bhrother.zamro.pim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidUploadFileException extends RuntimeException {

    public InvalidUploadFileException(String expectedFormat) {
        super("Invalid format of file to upload, expected: " + expectedFormat);
    }
}
