package com.bhrother.zamro.pim.batch;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties("batch.files")
public class BatchProperties {
    Integer linesToSkip;
    String location;
}
