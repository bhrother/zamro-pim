package com.bhrother.zamro.pim.batch.processor;

import com.bhrother.zamro.pim.dto.CSVCategoryDTO;
import com.bhrother.zamro.pim.model.Category;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class CategoryProcessor implements ItemProcessor<CSVCategoryDTO, Category> {
    private static final Logger log = LoggerFactory.getLogger(CategoryProcessor.class);

    @Override
    public Category process(final CSVCategoryDTO csvCategoryDTO) {
        log.debug("Read: {}", csvCategoryDTO.toString());
        Category category = new Category();
        category.setCode(csvCategoryDTO.getCategoryId());
        category.setName(clearHtmlTags(csvCategoryDTO.getName()));
        log.debug("Converted to: {}", category.toString());

        return category;
    }

    private String clearHtmlTags(String textToClean) {
        return Jsoup.parse(textToClean).text();
    }
}