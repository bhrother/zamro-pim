package com.bhrother.zamro.pim.batch;

import com.bhrother.zamro.pim.batch.processor.ProductProcessor;
import com.bhrother.zamro.pim.dto.CSVProductDTO;
import com.bhrother.zamro.pim.model.Product;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.IncorrectTokenCountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManagerFactory;
import java.nio.file.Paths;

@Configuration
public class ProductBatchConfig {

    public static final String PRODUCT_FILE_NAME = "ProductData.csv";

    private final ApplicationContext applicationContext;
    private final BatchProperties batchProperties;

    public static final String[] HEADER = new String[] {
            "ZamroID", "Name", "Description", "MinOrderQuantity", "UnitOfMeasure", "CategoryID", "PurchasePrice", "Available"
    };

    @Autowired
    public ProductBatchConfig(ApplicationContext applicationContext,
                              BatchProperties batchProperties) {
        this.applicationContext = applicationContext;
        this.batchProperties = batchProperties;
    }

    @Bean
    @StepScope
    public FlatFileItemReader<CSVProductDTO> csvProductReader(@Value("#{jobParameters['fileName']}") String fileName) {
        String resourceFileName = Paths.get(batchProperties.location, PRODUCT_FILE_NAME).toString();
        if (!StringUtils.isEmpty(fileName)) {
            resourceFileName = fileName;
        }
        FlatFileItemReader<CSVProductDTO> reader = new FlatFileItemReader<>();
        reader.setStrict(false);
        reader.setLinesToSkip(batchProperties.linesToSkip);
        reader.setResource(applicationContext.getResource(resourceFileName));
        reader.setLineMapper(new ProductLineMapper());
        return reader;
    }

    @Bean
    public JpaItemWriter<Product> csvProductWriter(EntityManagerFactory entityManagerFactory) {
        JpaItemWriter<Product> itemWriter = new JpaItemWriter<>();
        itemWriter.setEntityManagerFactory(entityManagerFactory);
        return itemWriter;
    }

    @Bean
    public Step csvProductFileToDatabaseStep(StepBuilderFactory stepBuilderFactory,
                                             FlatFileItemReader<CSVProductDTO> csvProductReader,
                                             ProductProcessor productProcessor,
                                             JpaItemWriter<Product> csvProductWriter) {
        return stepBuilderFactory.get("csvProductFileToDatabaseStep")
                .<CSVProductDTO, Product>chunk(1000)
                .faultTolerant()
                .skip(IncorrectTokenCountException.class)
                .skipPolicy(new AlwaysSkipItemSkipPolicy())
                .reader(csvProductReader)
                .processor(productProcessor)
                .writer(csvProductWriter)
                .build();
    }

    private class ProductLineMapper extends DefaultLineMapper<CSVProductDTO> {
        {
            setLineTokenizer(new DelimitedLineTokenizer() {
                {
                    setNames(HEADER);
                }
            });
            setFieldSetMapper(new BeanWrapperFieldSetMapper<CSVProductDTO>() {
                {
                    setTargetType(CSVProductDTO.class);
                }
            });
        }
    }
}
