package com.bhrother.zamro.pim.batch;

import com.bhrother.zamro.pim.batch.processor.CategoryProcessor;
import com.bhrother.zamro.pim.cache.CacheConfig;
import com.bhrother.zamro.pim.dto.CSVCategoryDTO;
import com.bhrother.zamro.pim.model.Category;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.IncorrectTokenCountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManagerFactory;
import java.nio.file.Paths;

@Configuration
public class CategoryBatchConfig {

    public static final String CATEGORY_FILE_NAME = "CategoryData.csv";

    private final ApplicationContext applicationContext;
    private final BatchProperties batchProperties;

    @Autowired
    public CategoryBatchConfig(ApplicationContext applicationContext,
                               BatchProperties batchProperties) {
        this.applicationContext = applicationContext;
        this.batchProperties = batchProperties;
    }

    @Bean
    @StepScope
    @CacheEvict(cacheNames= CacheConfig.CATEGORY_CACHE_NAME, allEntries=true)
    public FlatFileItemReader<CSVCategoryDTO> csvCategoryReader(@Value("#{jobParameters['fileName']}") String fileName) {
        String resourceFileName = Paths.get(batchProperties.location, CATEGORY_FILE_NAME).toString();
        if (!StringUtils.isEmpty(fileName)) {
            resourceFileName = fileName;
        }
        FlatFileItemReader<CSVCategoryDTO> reader = new FlatFileItemReader<>();
        reader.setStrict(false);
        reader.setLinesToSkip(batchProperties.linesToSkip);
        reader.setResource(applicationContext.getResource(resourceFileName));
        reader.setLineMapper(new CategoryLineMapper());
        return reader;
    }

    @Bean
    public JpaItemWriter<Category> csvCategoryWriter(EntityManagerFactory entityManagerFactory) {
        JpaItemWriter<Category> itemWriter = new JpaItemWriter<>();
        itemWriter.setEntityManagerFactory(entityManagerFactory);
        return itemWriter;
    }

    @Bean
    public Step csvCategoryFileToDatabaseStep(StepBuilderFactory stepBuilderFactory,
                                      FlatFileItemReader<CSVCategoryDTO> csvCategoryReader,
                                      CategoryProcessor categoryProcessor,
                                      JpaItemWriter<Category> csvCategoryWriter) {
        return stepBuilderFactory.get("csvCategoryFileToDatabaseStep")
                .<CSVCategoryDTO, Category>chunk(1)
                .faultTolerant()
                .skip(IncorrectTokenCountException.class)
                .skipPolicy(new AlwaysSkipItemSkipPolicy())
                .reader(csvCategoryReader)
                .processor(categoryProcessor)
                .writer(csvCategoryWriter)
                .build();
    }


    private class CategoryLineMapper extends DefaultLineMapper<CSVCategoryDTO> {
        {
            setLineTokenizer(new DelimitedLineTokenizer() {
                {
                    setNames("categoryId", "name");
                }
            });
            setFieldSetMapper(new BeanWrapperFieldSetMapper<CSVCategoryDTO>() {
                {
                    setTargetType(CSVCategoryDTO.class);
                }
            });
        }
    }
}
