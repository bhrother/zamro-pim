package com.bhrother.zamro.pim.batch.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobsConfig {

    @Bean
    public JobExecutionListener jobExecutionListener() {
        return new JobExecutionListener() {
            private final Logger logger = LoggerFactory.getLogger(JobExecutionListener.class);
            @Override
            public void beforeJob(JobExecution jobExecution) {
                logger.debug("Before " + jobExecution.toString());
            }

            @Override
            public void afterJob(JobExecution jobExecution) {
                logger.debug("After " + jobExecution.toString());
            }
        };
    }

    /**
     * Responsible for creating the job.
     *
     * @param jobBuilderFactory
     * @param jobExecutionListener
     * @param csvCategoryFileToDatabaseStep {@link com.bhrother.zamro.pim.batch.CategoryBatchConfig}
     * @param csvProductFileToDatabaseStep {@link com.bhrother.zamro.pim.batch.ProductBatchConfig}
     * @return
     */
    @Bean
    Job csvFileToDatabaseJob(JobBuilderFactory jobBuilderFactory,
                             JobExecutionListener jobExecutionListener,
                             Step csvCategoryFileToDatabaseStep,
                             Step csvProductFileToDatabaseStep) {
        return jobBuilderFactory.get("csvFileToDatabaseJob")
                .incrementer(new RunIdIncrementer())
                .listener(jobExecutionListener)
                .start(csvCategoryFileToDatabaseStep)
                .next(csvProductFileToDatabaseStep)
                .build();
    }

    @Bean
    Job csvProductFileToDatabaseJob(JobBuilderFactory jobBuilderFactory,
                             JobExecutionListener jobExecutionListener,
                             Step csvProductFileToDatabaseStep) {
        return jobBuilderFactory.get("csvProductFileToDatabaseJob")
                .incrementer(new RunIdIncrementer())
                .listener(jobExecutionListener)
                .start(csvProductFileToDatabaseStep)
                .build();
    }

    @Bean
    Job csvCategoryFileToDatabaseJob(JobBuilderFactory jobBuilderFactory,
                             JobExecutionListener jobExecutionListener,
                             Step csvCategoryFileToDatabaseStep) {
        return jobBuilderFactory.get("csvCategoryFileToDatabaseJob")
                .incrementer(new RunIdIncrementer())
                .listener(jobExecutionListener)
                .start(csvCategoryFileToDatabaseStep)
                .build();
    }

}
