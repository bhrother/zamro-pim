package com.bhrother.zamro.pim.batch.processor;

import com.bhrother.zamro.pim.dto.CSVProductDTO;
import com.bhrother.zamro.pim.exception.InvalidCSVProductException;
import com.bhrother.zamro.pim.model.Product;
import com.bhrother.zamro.pim.service.CategoryService;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ProductProcessor implements ItemProcessor<CSVProductDTO, Product> {
    private static final Logger log = LoggerFactory.getLogger(ProductProcessor.class);

    private final CategoryService categoryService;

    @Autowired
    public ProductProcessor(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public Product process(final CSVProductDTO csvProductDTO) {
        log.debug("Read: {}", csvProductDTO.toString());
        if (isValidProduct(csvProductDTO)) {
            Product product = new Product();
            product.setZamroID(csvProductDTO.getZamroId());
            product.setName(clearHtmlTags(csvProductDTO.getName()));
            product.setDescription(clearHtmlTags(csvProductDTO.getDescription()));
            product.setMinOrderQuantity(csvProductDTO.getMinOrderQuantity());
            product.setPurchasePrice(csvProductDTO.getPurchasePrice());
            product.setAvailable(csvProductDTO.getAvailable() == 1);
            product.setUnitOfMeasure(clearHtmlTags(csvProductDTO.getUnitOfMeasure()));

            product.setCategory(categoryService.findByCode(csvProductDTO.getCategoryID()));

            log.debug("Converted to: {}", product.toString());
            return product;
        }
        log.error("Product does not contains all required fields, ignored.");
        throw new InvalidCSVProductException();
    }

    private boolean isValidProduct(final CSVProductDTO CSVProductDTO) {
        return (!StringUtils.isEmpty(CSVProductDTO.getZamroId()) &&
                !StringUtils.isEmpty(CSVProductDTO.getName()) &&
                CSVProductDTO.getAvailable() != null &&
                CSVProductDTO.getPurchasePrice() != null &&
                CSVProductDTO.getUnitOfMeasure() != null &&
                CSVProductDTO.getCategoryID() != null
        );
    }

    private String clearHtmlTags(String textToClean) {
        return Jsoup.parse(textToClean).text();
    }
}