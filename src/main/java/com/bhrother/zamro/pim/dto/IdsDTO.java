package com.bhrother.zamro.pim.dto;

import lombok.Data;

import java.util.Set;

@Data
public class IdsDTO {
    Set<Long> ids;
}
