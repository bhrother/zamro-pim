package com.bhrother.zamro.pim.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CSVProductDTO {
    private String zamroId;
    private String name;
    private String description;
    private BigDecimal minOrderQuantity;
    private String unitOfMeasure;
    private Long categoryID;
    private BigDecimal purchasePrice;
    private Integer available;

    public static class Builder {

        CSVProductDTO csvProductDTO;

        public Builder() {
            csvProductDTO = new CSVProductDTO();
        }

        public Builder zamroId(String zamroId) {
            csvProductDTO.setZamroId(zamroId);
            return this;
        }
        public Builder name(String name) {
            csvProductDTO.setName(name);
            return this;
        }
        public Builder description(String description) {
            csvProductDTO.setDescription(description);
            return this;
        }
        public Builder minOrderQuantity(BigDecimal minOrderQuantity) {
            csvProductDTO.setMinOrderQuantity(minOrderQuantity);
            return this;
        }
        public Builder unitOfMeasure(String unitOfMeasure) {
            csvProductDTO.setUnitOfMeasure(unitOfMeasure);
            return this;
        }
        public Builder categoryID(Long categoryID) {
            csvProductDTO.setCategoryID(categoryID);
            return this;
        }
        public Builder purchasePrice(BigDecimal purchasePrice) {
            csvProductDTO.setPurchasePrice(purchasePrice);
            return this;
        }
        public Builder available(Integer available) {
            csvProductDTO.setAvailable(available);
            return this;
        }

        public CSVProductDTO build() {
            return csvProductDTO;
        }
    }
}
