package com.bhrother.zamro.pim.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CSVCategoryDTO {
    private Long categoryId;
    private String name;

    public static class Builder {
        CSVCategoryDTO csvCategoryDTO;

        public Builder() {
            csvCategoryDTO = new CSVCategoryDTO();
        }

        public Builder categoryId(Long categoryId) {
            csvCategoryDTO.setCategoryId(categoryId);
            return this;
        }

        public Builder name(String name) {
            csvCategoryDTO.setName(name);
            return this;
        }

        public CSVCategoryDTO build() {
            return csvCategoryDTO;
        }
    }
}
