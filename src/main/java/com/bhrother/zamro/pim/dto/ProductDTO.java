package com.bhrother.zamro.pim.dto;

import com.bhrother.zamro.pim.model.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ProductDTO {
    private Long id;
    private String zamroId;
    private String name;
    private String description;
    private BigDecimal minOrderQuantity;
    private String unitOfMeasure;
    private CategoryDTO categoryDTO;
    private BigDecimal purchasePrice;
    private Boolean available;


    public static ProductDTO fromProduct(Product product) {
        return new Builder()
                .id(product.getId())
                .zamroId(product.getZamroID())
                .name(product.getName())
                .description(product.getDescription())
                .categoryDTO(CategoryDTO.from(product.getCategory()))
                .minOrderQuantity(product.getMinOrderQuantity())
                .unitOfMeasure(product.getUnitOfMeasure())
                .purchasePrice(product.getPurchasePrice())
                .available(product.getAvailable())
                .build();
    }

    public Product toProduct() {
        Product product = new Product();
        product.setId(product.getId());
        product.setZamroID(this.getZamroId());
        product.setName(this.getName());
        product.setDescription(this.getDescription());
        product.setMinOrderQuantity(this.getMinOrderQuantity());
        product.setPurchasePrice(this.getPurchasePrice());
        product.setAvailable(this.getAvailable());
        product.setUnitOfMeasure(this.getUnitOfMeasure());
        product.setCategory(categoryDTO.toCategory());

        return product;
    }

    public static class Builder {

        ProductDTO productDTO;

        public Builder() {
            productDTO = new ProductDTO();
        }

        public Builder id(Long id) {
            productDTO.setId(id);
            return this;
        }

        public Builder zamroId(String zamroId) {
            productDTO.setZamroId(zamroId);
            return this;
        }
        public Builder name(String name) {
            productDTO.setName(name);
            return this;
        }
        public Builder description(String description) {
            productDTO.setDescription(description);
            return this;
        }
        public Builder minOrderQuantity(BigDecimal minOrderQuantity) {
            productDTO.setMinOrderQuantity(minOrderQuantity);
            return this;
        }
        public Builder unitOfMeasure(String unitOfMeasure) {
            productDTO.setUnitOfMeasure(unitOfMeasure);
            return this;
        }
        public Builder categoryDTO(CategoryDTO categoryDTO) {
            productDTO.setCategoryDTO(categoryDTO);
            return this;
        }
        public Builder purchasePrice(BigDecimal purchasePrice) {
            productDTO.setPurchasePrice(purchasePrice);
            return this;
        }
        public Builder available(boolean available) {
            productDTO.setAvailable(available);
            return this;
        }

        public ProductDTO build() {
            return productDTO;
        }
    }
}
