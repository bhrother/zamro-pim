package com.bhrother.zamro.pim.dto;

import com.bhrother.zamro.pim.model.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;

@Data
@NoArgsConstructor
public class CategoryDTO {
    private Long id;
    private Long code;
    private String name;

    public static CategoryDTO from(Category category) {
        return new Builder()
                .id(category.getId())
                .code(category.getCode())
                .name(category.getName())
                .build();
    }

    public Category toCategory() {
        return new Category(this.getId(), this.getCode(), this.getName(), Collections.emptySet());
    }

    public static class Builder {
        CategoryDTO categoryDTO;

        public Builder() {
            categoryDTO = new CategoryDTO();
        }


        public Builder id(Long id) {
            categoryDTO.setId(id);
            return this;
        }

        public Builder code(Long code) {
            categoryDTO.setCode(code);
            return this;
        }

        Builder name(String name) {
            categoryDTO.setName(name);
            return this;
        }

        public CategoryDTO build() {
            return categoryDTO;
        }
    }
}
