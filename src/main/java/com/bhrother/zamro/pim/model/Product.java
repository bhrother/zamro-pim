package com.bhrother.zamro.pim.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@Data
@Entity(name = "products")
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String zamroID;
    private String name;
    private String description;
    private BigDecimal minOrderQuantity;
    private String unitOfMeasure;
    private BigDecimal purchasePrice;
    private Boolean available;
    @ManyToOne
    private Category category;
}
