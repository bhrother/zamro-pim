package com.bhrother.zamro.pim.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "categories")
public class Category {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private Long code;
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<Product> products;

    public static Category fromId(Long id) {
        Category category = new Category();
        category.setId(id);
        return category;
    }
}
