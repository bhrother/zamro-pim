package com.bhrother.zamro.pim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class ZamroPimApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZamroPimApplication.class, args);
	}
}
