# Zamro PIM example

This project it is a simple version of a Product Information Management (PIM).
GZIP it is enable for all responses. Check application.yml file.

## Technologies

- Spring-Boot 2.0.2
- Spring Framework 5.0.6
- Spring Batch 4.0.1
- Lombok 1.16.22
- Jsoup 1.11.3
- Apache POI 3.17

## How to build the application

```
./gradlew clean build
```

###How to run the application
```
java -jar build/libs/zamro-pim-0.0.1-SNAPSHOT.jar
```

To run with CategoryData and ProductData imported

```
java -Dspring.profiles.active=local -jar build/libs/zamro-pim-0.0.1-SNAPSHOT.jar
```

To check if it is running, try to access the URL:

```
http://localhost:8081/actuator/health
```

## Rest Endpoints

After start the application, you can check the Swagger documentation at:
```
http://localhost:8081/swagger-ui.html
```
